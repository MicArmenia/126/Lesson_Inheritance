﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            var st = new Student
            {
                name = "A1",
                surname = "A1yan",
                email = "a1@gmail.com",
                faculty = "faculty"
            };

            var t = new Teacher
            {
                name = "T1",
                surname = "T1yan",
                email = "t1@gmail.com",
                salary = 25000
            };

            Print(st);
            Console.WriteLine();
            Print(t);

            Console.ReadLine();
        }

        static void Print(Student model)
        {
            Console.WriteLine("****** Student ******");
            Console.WriteLine(model.FullName);
            Console.WriteLine($"email: {model.email}");
        }

        static void Print(Teacher model)
        {
            Console.WriteLine("****** Teacher ******");
            Console.WriteLine(model.FullName);
            Console.WriteLine($"email: {model.email}");
        }
    }
}
