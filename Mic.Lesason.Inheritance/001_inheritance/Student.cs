﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_inheritance
{
    class Student
    {
        public string name;
        public string surname;
        public string email;

        public string FullName => $"{surname} {name}";
        //{
        //    get { return $"{surname} {name}"; }
        //}

        public string faculty;
    }
}
