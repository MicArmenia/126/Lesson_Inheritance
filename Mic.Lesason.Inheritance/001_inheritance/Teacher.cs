﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_inheritance
{
    class Teacher
    {
        public string name;
        public string surname;
        public string email;

        public string FullName => $"{surname} {name}";

        public int salary;
    }
}