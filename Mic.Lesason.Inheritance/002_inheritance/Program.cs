﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            var st = new Student
            {
                name = "A1",
                surname = "A1yan",
                email = "a1@gmail.com",
                faculty = "faculty"
            };

            Person p1 = st;

            var t = new Teacher
            {
                name = "T1",
                surname = "T1yan",
                email = "t1@gmail.com",
                salary = 25000
            };

            Person p2 = t;

            Print(st);
            Console.WriteLine();
            Print(t);

            Console.ReadLine();
        }

        static void Print(Person model)
        {
            string typeName = model.GetType().Name;

            Console.WriteLine($"****** {typeName} ******");
            Console.WriteLine(model.FullName);
            Console.WriteLine($"email: {model.email}");
        }
    }
}
