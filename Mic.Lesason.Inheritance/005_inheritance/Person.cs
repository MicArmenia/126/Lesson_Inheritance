﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_inheritance
{
    class Person
    {
        public string name;
        public string surname;
        public string email;

        public string FullName => $"{surname} {name}";

        public Person Copy()
        {
            return new Person
            {
                name = this.name,
                surname = this.surname,
                email = this.email
            };

            //Person p1 = new Person
            //{
            //    name = this.name,
            //    surname = this.surname,
            //    email = this.email
            //};

            //return p1;
        }
    }
}