﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person
            {
                name = "A1",
                surname = "A1yan",
                email = "a1@gmail.com"
            };

            //Person p1 = new Person
            //{
            //    name = p.name,
            //    surname = p.surname,
            //    email = p.email
            //};

            Person p1 = p.Copy();

        }
    }
}
