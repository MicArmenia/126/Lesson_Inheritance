﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_inheritance
{
    class Person
    {
        public Person()
        {
            test = new Test();
        }

        public string name;
        public string surname;
        public string email;

        public string FullName => $"{surname} {name}";

        public Test test;

        //public Person Copy()
        //{
        //    object obj = MemberwiseClone();
        //    return (Person)obj;
        //}

        public Person Copy()
        {
            return MemberwiseClone() as Person;
        }
    }

    class Test
    {

    }
}