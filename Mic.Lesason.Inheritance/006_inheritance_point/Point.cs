﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_inheritance_point
{
    class Point
    {
        public int x;
        public int y;

        public void PrintPoint()
        {
            Console.WriteLine($"({x},{y})");
        }
    }
}