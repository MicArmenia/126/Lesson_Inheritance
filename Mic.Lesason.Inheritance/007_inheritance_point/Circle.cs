﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_inheritance_point
{
    class Circle : Point
    {
        public Circle()
        {
            r = 5;
        }

        private int r;
        public int R
        {
            get => r;
            //get { return r; }
            set => r = value < 0 ? 5 : value;
            //{
            //    r = value < 0 ? 5 : value;
            //    if (value < 0)
            //        r = 5;
            //    else
            //        r = value;
            //}
        }

        public void PrintCircle()
        {
            Console.WriteLine($"({x},{y}), R = {R}");
        }
    }
}