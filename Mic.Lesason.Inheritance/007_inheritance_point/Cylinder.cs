﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_inheritance_point
{
    class Cylinder : Circle
    {
        public Cylinder()
        {
            h = 5;
        }

        private int h;
        public int H
        {
            get => h;
            set => h = value < 0 ? 5 : value;
        }

        public void PrintCylinder()
        {
            Console.WriteLine($"({x},{y}), R = {R}, H = {H}");
        }
    }
}
