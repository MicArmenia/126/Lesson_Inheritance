﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_inheritance_point
{
    class Circle : Point
    {
        public Circle()
        {
            r = 5;
        }

        private int r;
        public int R
        {
            get => r;
            set => r = value < 0 ? 5 : value;
        }

        public new void Print()
        {
            Console.WriteLine($"({x},{y}), R = {R}");
        }
    }
}