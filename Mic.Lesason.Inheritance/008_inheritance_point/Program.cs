﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_inheritance_point
{
    class Program
    {
        static void Main(string[] args)
        {
            Point p = new Point { x = 10, y = 15 };
            p.Print();

            Circle c = new Circle { x = -15, y = 100, R = -10 };
            c.Print();

            Cylinder cylinder = new Cylinder { x = -105, y = 77, R = 20, H = -10 };
            cylinder.Print();

            Console.ReadLine();
        }
    }
}
