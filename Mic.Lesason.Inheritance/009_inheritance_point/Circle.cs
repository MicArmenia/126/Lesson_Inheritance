﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_inheritance_point
{
    /*sealed*/ class Circle : Point
    {
        public Circle()
        {
            r = 5;
        }

        private int r;
        public int R
        {
            get => r;
            set => r = value < 0 ? 5 : value;
        }

        public /*sealed*/ override void Print()
        {
            Console.WriteLine($"({x},{y}), R = {R}");
        }
    }
}