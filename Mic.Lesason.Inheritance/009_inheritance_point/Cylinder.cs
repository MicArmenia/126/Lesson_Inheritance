﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_inheritance_point
{
    class Cylinder : Circle
    {
        public Cylinder()
        {
            h = 5;
        }

        private int h;
        public int H
        {
            get => h;
            set => h = value < 0 ? 5 : value;
        }

        //0x404
        public override void Print()
        {
            Console.WriteLine($"({x},{y}), R = {R}, H = {H}");
        }
    }
}
