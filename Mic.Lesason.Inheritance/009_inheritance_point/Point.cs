﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_inheritance_point
{
    class Point
    {
        public int x;
        public int y;

        public virtual void Print()
        {
            Console.WriteLine($"({x},{y})");
        }
    }
}