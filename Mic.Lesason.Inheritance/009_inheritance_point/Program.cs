﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_inheritance_point
{
    class Program
    {
        static void Main(string[] args)
        {
            Point p = new Point { x = 10, y = 15 };
            //p.Print();
            Print(p);

            Circle c = new Circle { x = -15, y = 100, R = -10 };
            //c.Print();
            Print(c);

            Cylinder cylinder = new Cylinder { x = -105, y = 77, R = 20, H = -10 };
            //cylinder.Print();
            Print(cylinder);
            
            Console.ReadLine();
        }

        static void Print(Point p)
        {
            string header = $"***** {p.GetType().Name} *****";
            Console.WriteLine(header);
            Console.WriteLine();

            p.Print();

            Console.WriteLine();
            Console.WriteLine(new string('*', header.Length));
        }
    }
}
