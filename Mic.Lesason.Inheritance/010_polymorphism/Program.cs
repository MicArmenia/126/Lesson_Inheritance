﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _010_polymorphism
{
    class Program
    {
        static void Main(string[] args)
        {
            var st = new Student
            {
                name = "A1",
                surname = "A1yan",
                email = "a1@gmail.com",
                faculty = "faculty"
            };

            Console.WriteLine(st);
            Console.ReadLine();
        }
    }
}
