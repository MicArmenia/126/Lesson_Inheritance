﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _010_polymorphism
{
    class Student
    {
        public string name;
        public string surname;
        public string email;

        public string FullName => $"{surname} {name}";

        public string faculty;

        public override string ToString()
        {
            return FullName;
        }
    }
}
